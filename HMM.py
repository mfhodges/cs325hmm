# -*- coding: utf-8 -*-
import numpy as np
from hmmlearn import hmm
from Viterbi import *
import pickle


"""
# write python dict to a file
mydict = {'a': 1, 'b': 2, 'c': 3}
output = open('myfile.pkl', 'wb')
pickle.dump(mydict, output)
output.close()

# read python dict back from the file
pkl_file = open('myfile.pkl', 'rb')
mydict2 = pickle.load(pkl_file)
pkl_file.close()
"""

def runall(sent_align,word_align):
	with open(sent_align, "r") as es_file:
        	es_data = es_file.read().decode('latin1')

	English = []
	Spanish = []
	es_data = eval(es_data)
	for s in es_data[:-1]:
    		Spanish += [s[0].split()]
    		English += [s[1].split()]

	EnglishV = []
	for s1 in English:
    		EnglishV += s1
	EnglishV = list(set(EnglishV))

	SpanishV = []
	for s2 in Spanish:
    		SpanishV += s2
	SpanishV = list(set(SpanishV))

	testS_Source = "Nothing has changed"
	observations = testS_Source.split() #dont overlap words
	states = SpanishV


	startprobability=[]
	for state in states:
    		prob=0
    		for s in Spanish:
        		if s[0]==state:
            			prob+=1
    		prob = float(prob)
    		startprobability+=[prob]
	t = sum(startprobability)
	newstart=[w/t for w in startprobability]
	startprobability = np.array(newstart)

	transitionprob = []
	#likelihood of row i state goin to col j state
	for s1 in states:
    		row=[]
    		for s2 in states:
        		#row s1, col s2 (find incidence of s1 s2)
        		prob=0
        		total=0
        		for sentence in Spanish:
            			for i in range(0,len(sentence)):
                			if sentence[i]==s1 and i<len(sentence)-1 and sentence[i+1] in states:
                    				total+=1
                    				if i<len(sentence)-1:
                        				if sentence[i+1]==s2:
                            					prob+=1
        		if not total==0:
            			prob=float(prob)/total
        		row+=[prob]
    		transitionprob += [row]
	transitionprob = np.array(transitionprob)


	obs = tuple(observations)
	states = tuple(states)
	start_p = {}
	for i in range(0,len(states)):
    		start_p[states[i]] = startprobability[i]
	trans_p = {}
	for j in range(0,len(states)):
    		trans_p[states[j]] = {}
    		for k in range(0,len(states)):
        		trans_p[states[j]][states[k]] = transitionprob[j][k]


	with open(word_align, "r") as tdict:
		data = tdict.read()

	alignments = eval(data)

	vocab = []
	for key in alignments:
    		for k in alignments[key]:
        		vocab+=[k]

	vocab = list(set(vocab))

	for key1 in alignments:
    		for word in vocab:
        		if word not in alignments[key1].keys():
            			alignments[key1][word] = 0.0

	emit_p = alignments
	#print viterbi(obs,states,start_p,trans_p,emit_p)
	#states - tuple
	#start_p, trans_p, emit_p - dictionaries

	st=open("states.txt","w")
	st.write(str(states))
	st.close()

	output_start = open('start_p.pkl', 'wb')
	pickle.dump(start_p, output_start)
	output_start.close()

        output_trans = open('trans_p.pkl', 'wb')
        pickle.dump(trans_p, output_trans)
        output_trans.close()

        output_emit = open('emit_p.pkl', 'wb')
        pickle.dump(emit_p, output_emit)
        output_emit.close()

	return states,start_p,trans_p,emit_p


#runall("tuples.txt","results.txt")

def readall(directory):
	with open("states.txt", "r") as st:
        	states = st.read().decode('latin1')
	states = eval(states)
	print "loaded states"
	output_start = open("start_p.pkl", 'rb')
	start_p = pickle.load(output_start)
	output_start.close()

	print "loaded start_p"
        output_trans = open("trans_p.pkl", 'rb')
        trans_p = pickle.load(output_trans)
        output_trans.close()

	print "loaded trans_p"
        output_emit = open("emit_p.pkl", 'rb')
        emit_p = pickle.load(output_emit)
        output_emit.close()

	print "loaded emit_p"
	return states,start_p,trans_p,emit_p
