import sys
# from https://en.wikipedia.org/wiki/Viterbi_algorithm

obs = ('the', 'blue', 'house')
states = ('la', 'casa','azul')
start_p = {'la': 1.0, 'casa':0.0 ,'azul':0.0}
trans_p = {
   'la': {'la':0.02,'casa':0.8,'azul':0.18},
   'casa': {'la':0.02,'casa':0.03,'azul':0.95},
   'azul': {'la':0.3,'casa':0.6,'azul':0.1}
   }
emit_p = {
   'la': {'the': 0.7, 'blue': 0.2, 'house': 0.1},
   'casa': {'the': 0.1, 'blue': 0.1, 'house': 0.8},
   'azul': {'the': 0.05, 'blue': 0.75, 'house': 0.2},
   }


def viterbi(obs, states, start_p, trans_p, emit_p):
      V = [{}]
      for st in states:
	  try:
             V[0][st] = {"prob": start_p[st] * emit_p[st][obs[0]], "prev": None}
	  except KeyError:
	     print "sorry, %s is not in our vocab" % (obs[0])
	     return False
#	     sys.exit()	
      # Run Viterbi when t > 0
      for t in range(1, len(obs)):
          V.append({})
          for st in states:
             max_tr_prob = max(V[t-1][prev_st]["prob"]*trans_p[prev_st][st] for prev_st in states)
             for prev_st in states:
                 if V[t-1][prev_st]["prob"] * trans_p[prev_st][st] == max_tr_prob:
                     try:
		         max_prob = max_tr_prob * emit_p[st][obs[t]]
                         V[t][st] = {"prob": max_prob, "prev": prev_st}
                     except KeyError:
             		 print "sorry, %s is not in our vocab" % (obs[t])
             		 return False
                     break
      #for line in dptable(V):
       #   print line
      opt = []
     # The highest probability
      max_prob = max(value["prob"] for value in V[-1].values())
      previous = None
     # Get most probable state and its backtrack
      for st, data in V[-1].items():
          if data["prob"] == max_prob:
              opt.append(st)
              previous = st
              break
     # Follow the backtrack till the first observation
      for t in range(len(V) - 2, -1, -1):
          opt.insert(0, V[t + 1][previous]["prev"])
          previous = V[t + 1][previous]["prev"]

      print 'The steps of states are ' + ' '.join(opt) + ' with highest probability of %s' % max_prob
      return ' '.join(opt)
def dptable(V):
     # Print a table of steps from dictionary
     yield " ".join(("%12d" % i) for i in range(len(V)))
     for state in V[0]:
         yield "%.7s: " % state + " ".join("%.7s" % ("%f" % v[state]["prob"]) for v in V)

#print viterbi(obs,states,start_p,trans_p,emit_p)


SENTENCES = [
        ('mi casa verde'.split(), 'my green house'.split()),
        ('casa verde'.split(), 'green house'.split()),
        ('la casa'.split(), 'the house'.split()),
    ]


test_trans_p= {'green': {'casa': 3.8566856306875825e-13, 'verde': 0.9999999999996143, 'mi': 1.6128709473392621e-21}, 'the': {'casa': 1.6043357358486378e-27, 'la': 1.0}, 'my': {'casa': 1.2513850356565044e-27, 'verde': 3.2257418946785243e-21, 'mi': 1.0}, 'house': {'casa': 0.9999999999997429, 'verde': 2.5711237537917215e-13, 'mi': 4.171283452188348e-28, 'la': 5.347785786162126e-28}}
