from HMM import *
from Viterbi import *


f=open('tuples.txt','r')
sent_align=eval(f.read().decode('latin1'))


states,start_p,trans_p,emit_p=readall('Model')


result= open('hmmeval.txt','w')
correct = 0
for s in sent_align:
	english = s[1]
	spanish =s[0]
	trans = viterbi(tuple(s[1].split()), states,start_p,trans_p,emit_p)
	if trans == spanish:
		correct += 1
	else:
		print "failed:(%s,%s)\n" % (trans,spanish)
		result.write("failed:(%s,%s)\n" % (trans.encode("utf-8"),spanish.encode("utf-8")))	
result.write("Got %s correct out of %s" % (correct, len(sent_align)))
result.close()	



