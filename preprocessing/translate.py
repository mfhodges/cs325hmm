import goslate
import glob

#gs = goslate.Goslate()
#print(gs.translate('hello world', 'es'))

fo = open("spanishtrans.txt", "w")
en_t = open("englishtrans.txt", "w")

big_files = glob.glob("txtfiles/*.txt")
gs = goslate.Goslate()

translation = []
x=0
accepted = 0
for big_file in big_files:
	with open(big_file, 'r') as f:
		for line in f:
			translated_line = gs.translate(line,'es')
			if len(translated_line.split()) == len(line.split()):
				fo.write(translated_line.encode('utf-8')+'\n')
				accepted += 1
				en_t.write(line.encode('utf-8')+'\n')
			x+=1
			if x%100==0:
				print "accepted: %s from total: %s" % ( accepted, x)
en_t.close()
fo.close()

