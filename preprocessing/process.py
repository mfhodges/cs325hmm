# -*- coding: utf-8 -*-

import time
from collections import defaultdict
import copy
import itertools
import operator
import time
import sys
from nltk import tokenize
from xml.dom import minidom
import string
#from joblib import Parallel, delayed
#from progressbar import ProgressBar
#pbar = ProgressBar()

myfile=open("tuples2.txt","w") #opens file so we can write to it

"""
xmldoc = minidom.parse('NEWS.xml')
itemlist= xmldoc.getElementsByTagName('link')
print len(itemlist)
mapping = {}
for i in range(0,len(itemlist)):
    en, es = itemlist[i].attributes['xtargets'].value.split(';')
    if len(en)>0 and len(es)>0 and " " not in en and " " not in es:
        en = int(en)
        es = int(es)
        mapping[en] = es
print "finished mapping"
"""
#myfile.write(str(mapping))
"""
with open("en_euro_googletranslate.txt", "r") as en_file:
    en_data = en_file.read().decode('latin1')

#en_s = tokenize.sent_tokenize(en_data)
en_s = en_data.split("\n")
#myfile.write(str(en_s))


with open("es_euro_googletranslate.txt", "r") as es_file:
    es_data = es_file.read().decode('latin1')

#es_s = tokenize.sent_tokenize(es_data)
es_s = es_data.split("\n")
#print es_s[:5]
"""

#myfile.write(str(es_s))
print "finished ingesting txt files"
"""
with open("processingEnglish.txt", "r") as en_file:
    en_data = en_file.read().decode('latin1')
    #list of english sentences
en_S = eval(en_data)

with open("processingSpanish.txt", "r") as es_file:
    es_data = es_file.read().decode('latin1')
    #list of spanish sentences
es_S = eval(es_data)

with open("processing.txt", "r") as xmldict:
    xml_data = xmldict.read()
xml_S = eval(xml_data)
"""

#en_S = en_s
#es_S = es_s
#xml_S = mapping

"""
new = []
for key in xml_S:
    #we subtract one because indices of lists start at 0, not 1
    print key,xml_S[key]# so we know what's happening
    eI = int(key)-1
    sI = int(xml_S[key])-1
    englishS = en_S[eI]
    spanishS = es_S[sI]
    new+=[(englishS,spanishS)]

"""

"""
with open("tuples.txt", "r") as tdict:
    data = tdict.read()
sentences = eval(data)
"""

"""
sentences = zip(es_S,en_S)
print "finished aligning sentences by xml"

new2=[]
for i in range(0,len(sentences)):
    english=sentences[i][1]
    spanish=sentences[i][0]
    #new2+=[(spanish,english)]
    if len(english.split())==len(spanish.split()):
        new2+=[(spanish,english)]

print len(new2)
#for s in range(0,500):
#    myfile.write(sentences[s][1].encode('utf-8'))
#    myfile.write('\n')
"""


with open("tuples.txt", "r") as tdict:
    data = tdict.read()
sentences = eval(data)

#get rid of punctuation and strange symbols?
newsentences = []
for pair in sentences:
    es=pair[0][0:-2].translate(string.punctuation)
    en=pair[1][0:-2].translate(string.punctuation)
    es=es.translate("//")
    en=en.translate("//")
    newsentences+=[(es,en)]

"""
with open("processingEnglish.txt", "r") as en_file:
    en_data = en_file.read().decode('latin1')
    #list of english sentences
en_S = eval(en_data)

for s in en_S:
    myfile.write(s.encode('utf-8'))
    myfile.write('\n')

with open("processingSpanish.txt", "r") as es_file:
    es_data = es_file.read().decode('latin1')
    #list of spanish sentences
es_S = eval(es_data)

for s in es_S:
    myfile.write(s.encode('utf-8'))
    myfile.write('\n')
"""

#sentences = zip(es_S,en_S)
print "finished creating tuples"
#when you want to write to file!!
myfile.write(str(newsentences))
myfile.close()
