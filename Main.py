#!/usr/bin/python
from HMM import *
from Viterbi import *
import sys


 # import all py files

# NOTE: running the EM and translating are not handled here due to time required to calculate.
# See paper for more info 


def Main(someval):
	print someval, "helo"
	if someval == 0:  #starting from scratch
		# make model from sentence and word alignments and store output in files
		print "executing runall()"
		states, start_p, trans_p,emit_p = runall("new_tuples.txt","new_results.txt") # in hmmtest makes parameters for Viterbi

		print "finished runall()"
	elif someval ==1: # loading hmm from files created from runall 
		#read in files ( the model ) to pass to Viterbi
		print "executing readall()"
		states, start_p, trans_p,emit_p = readall("Model") # takes directory name where the files will exist..
	else:
		print "parameters are 0(make model) or 1(load model)!"
		sys.exit()#Throw error

	x = None
	print "enter the english sentence you want translated without any quotation marks or punctuation. Enter end to terminate"
	while x != 'end':
		x = raw_input("\tenglish sentence:")
		if x == 'end':
			break
		print "\n"
		val=viterbi(tuple(x.split()), states, start_p, trans_p, emit_p)   
		print " fun! lets do another \n"


Main(int(sys.argv[1]))
