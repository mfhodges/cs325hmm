# -*- coding: utf-8 -*-


# IBM Model 1 EM algorithm for predicting word alignments


import time
from collections import defaultdict
import copy
import itertools
import operator
import time
import sys
from nltk import tokenize
from xml.dom import minidom
import sys
import pprint

myfile=open("results.txt","w")
#print 'Argument List:', str(sys.argv)

def em(sent_pairs):
          source_sent, target_sent = zip(*sent_pairs)
          source_vocab = set(itertools.chain.from_iterable(source_sent))
          target_vocab = set(itertools.chain.from_iterable(target_sent))
#	  print "finished vocab and sent unzip"
#	  myfile.write("finished vocab and sent unzip \n")
          uniform_p = 1.0/len(source_vocab)
          cond_probs_old = None
          cond_probs = {(w_source,w_target): uniform_p
                        for w_source in source_vocab
                        for w_target in target_vocab}
          print "finished cond_probs"
#	  myfile.write("finished cond_probs \n")
          alignments = []
	  count = 0
	  t = len(sent_pairs)
          for source, target in sent_pairs:
               print count
               sub = []
               ta = time.clock()
               for t_p in itertools.permutations(target):
                    sub += [zip(source, t_p)]
               alignments += [sub]
	       count += 1
	       atime = time.clock()-ta
	       print " %s out of %s --- took: "  % (count,t), atime
#	       myfile.write(" %s out of %s --- took: %s \n"  % (count,t, atime))
	  print "STARTING WHILE LOOP"
#	  myfile.write("starting while loop \n")
          #repeat until convergence
          x = 0
	  max_change = 1
	  epsilon = 0.1
#          while cond_probs_old !=  cond_probs:
	  while max_change >= epsilon:
	       tx = time.clock()
               cond_probs_old = copy.copy(cond_probs)

	       alignment_probs= {}
               for i, sentence_alignments in enumerate(alignments):
                    alignment_probs[i] = None
                    sub = {}
                    for alignment in sentence_alignments:
                         value = reduce(operator.mul, [cond_probs[pair] for pair in alignment])
                         sub[tuple(alignment)]=value
                    alignment_probs[i] = sub
#               print "align_probs", alignment_probs
               #now to normalize
	       #print "normalizing"
               for s_index, s_alignments in alignment_probs.iteritems():
                    total= float(sum(s_alignments.values()))
                    probs = {}
                    for align, value in s_alignments.iteritems():
                         probs[align] = value/total
                    alignment_probs[s_index] = probs


               #maximization step
               #print "maximizing"
               w_trans = defaultdict(lambda: defaultdict(float))
               for sentence_a in alignment_probs.itervalues():
                    for w_pairs, prob in sentence_a.iteritems():
                         for source_w, target_w in w_pairs:
                              w_trans[target_w][source_w] += prob

               cond_probs = {}
               for target_w, trans in w_trans.iteritems():
                    total = float(sum(trans.values()))
                    for source_w, score in trans.iteritems():
                         cond_probs[source_w, target_w] = score/total

	       end = time.clock()-tx
	#       myfile.write("loop %s took: %s \n" % (x, end))
	       print "loop %s took: %s" % (x, end)
#	       myfile.write("loop %s took: %s \n" % (x, end))
	 #      print "loop %s took: %s" % (x, end)
	       x+=1


	       max_change = max([abs(cond_probs[k] - cond_probs_old[k]) for k in cond_probs.keys()])
#	       print "max change", max_change
	       #print '\n heres the cond_probs: \n', cond_probs
	  print "took", x,"loops to converge"
          return cond_probs





def format(emres):
	alignments = {}
	allkeys = emres.keys()
	spanishwords = set(map(lambda x: x[0], allkeys))
	for s_w in spanishwords:

		paired = filter(lambda x: x[0] == s_w, emres)

		alignments[s_w] = {}
		temp ={}
		for x in paired:
 			temp[x[1]]= emres[x]
		alignments[s_w] = temp
	return alignments




SENTENCES = [
        ('mi casa verde'.split(), 'my green house'.split()),
        ('casa verde'.split(), 'green house'.split()),
        ('la casa'.split(), 'the house'.split()),
    ]



SENTENCES2 = [("the big man walked to the house".split(), "el hombre grande camino a la casa".split()),
		("the blue bird is in the tree".split(), "el pajaro azul es en el arbol".split()),
		("my grandmother lives in the forest".split(), "mi abuela vive en el bosque".split())]




"""
=======
#ingesting the data
>>>>>>> 7d214af4f4eff1176202387e99cc998cbdf45244
tr = time.clock()
xmldoc = minidom.parse('en-es-Book.xml')
itemlist= xmldoc.getElementsByTagName('link')
print len(itemlist)
mapping = {}
for s in itemlist[:100]:
     en, es = s.attributes['xtargets'].value.split(';')
     mapping[en] = es
#     print s.attributes['xtargets'].value, s.attributes['id'].value
#print mapping


with open("SS_English.txt", "r") as en_file:
	en_data = en_file.read()

en_s = tokenize.sent_tokenize(en_data)
#print en_s[:5]

with open("SS_Spanish.txt", "r") as es_file:
        es_data = es_file.read().decode('latin1')

es_s = tokenize.sent_tokenize(es_data)
#print es_s[:5]

sentences = zip(es_s,en_s)
#print '\n \n'

split_s = list(map(lambda x:(x[0].split(), x[1].split()), sentences))

#considering sentences of size 7
filter_s = list(filter(lambda x : len(x[0]) == len(x[1]) and len(x[0])< 11, split_s))
#print '\n \n \n filter'
#print len(filter_s) , len(split_s)
#print filter_s[:4]

rtime =time.clock()-tr
print "took: %s to read files" % (rtime)
myfile.write("took: %s to read files \n" % (rtime))
t0 = time.clock()

#print "len f_s", len(filter_s)
"""

print "starting the alg!"
print "input:"

with open("tuples2.txt", "r") as es_file:
        es_data = es_file.read().decode('latin1')

es_data = eval(es_data)
new_data = []
for s in es_data[:-1]:
    new_data += [(s[0].split(),s[1].split())]

if str(sys.argv[1]) == 'test1':
	print SENTENCES
	res = em(SENTENCES)
elif str(sys.argv[1]) == 'test2':
	print SENTENCES2
	res = em(SENTENCES2)
elif str(sys.argv[1]) == 'becky':
	res = em(new_data)
else:
	sys.exit("you probably forgot the commandline argument")

print "\n Here's the result"
print pprint.pprint(res)

qwer= format(res) # this is the trans_probs

print '\n'
print " Here's the e_probs"
print pprint.pprint(qwer)
myfile.write(str(qwer))

#ftime = time.clock() - t0
#print "took: %s seconds" % (ftime)
#myfile.write("took: %s seconds \n" % (ftime))
#myfile.write('\n'+str(res)+'\n')
myfile.close()

#filtered = dict((key[0],key[1]) for key, value in res.iteritems() if value==1.0)
#print '\n', filtered
