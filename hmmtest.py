# -*- coding: utf-8 -*-
import numpy as np
from hmmlearn import hmm
from Viterbi import *
from processresults import *

"""
English = [
["the","big","man","walked","from","the","forest","to","the","house"],
["the","blue","bird","from","the","casa","is","in","the","tree"],
["my","grandmother","lives","in","the","forest","in","the united states"],
["the","blue","house","is","big"]
]

Spanish=[
["el","hombre","grande","caminó","desde","el","bosque","a","la","casa"],
["el","pájaro","azul","desde","la","casa","es","en","el","árbol"],
["mi","abuela","vive","en","el","bosque","en","los estados unidos"],
["la","casa","azul","es","grande"],
]

a={
0:{0:0,1:2,2:1,3:3,4:4,5:5,6:6,7:7,8:8,9:9},
1:{0:0,1:2,2:1,3:3,4:4,5:5,6:6,7:7,8:8,9:9},
2:{0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7},
3:{0:0,1:2,2:1,3:3,4:4}
}
"""





with open("tuples2.txt", "r") as es_file:
        es_data = es_file.read().decode('latin1')

English = []
Spanish = []
es_data = eval(es_data)
for s in es_data[:-1]:
    Spanish += [s[0].split()]
    English += [s[1].split()]


#NEW TEST SENTENCES
#English = ["the big man walked to the house".split(),"the blue bird is in the tree".split(),"my grandmother lives in the forest".split()]
#Spanish = ["el hombre grande camino a la casa".split(),"el pajaro azul es en el arbol".split(),"mi abuela vive en el bosque".split()]
#English = ['my green house'.split(),'green house'.split(),'the house'.split()]
#Spanish= ['mi casa verde'.split(),'casa verde'.split(),'la casa'.split()]

#assuming the sentences are aligned

#form: {index:{index1:index2,....}}
#Now im gonng do some post-processing of this training data so that the complexity is not wild later!!

EnglishV = []
for s1 in English:
    EnglishV += s1
EnglishV = list(set(EnglishV))

SpanishV = []
for s2 in Spanish:
    SpanishV += s2
SpanishV = list(set(SpanishV))

#print EnglishV,SpanishV
"""
alignments={}
counter={}
totals={}
for sw in SpanishV:
    alignments[sw] = {}
    counter[sw]=0
    totals[sw]=0
    for ew in EnglishV:
        alignments[sw][ew]=0

for c in range(0,len(Spanish)):
    for d in range(0,len(Spanish[c])):
        word=Spanish[c][d]
        alignedword=a[c][d]
        word2 = English[c][alignedword]
        alignments[word][word2]+=1
        #totals[word]+=1
"""
#test sentence: "the blue tree house is in the forest"

testS_Source = "Nothing has changed"
observations = testS_Source.split() #dont overlap words
states = SpanishV
#states = testS_Target.split()

startprobability=[]
for state in states:
    prob=0
    for s in Spanish:
        if s[0]==state:
            prob+=1
    prob = float(prob)
    startprobability+=[prob]
t = sum(startprobability)
newstart=[w/t for w in startprobability]
startprobability = np.array(newstart)

transitionprob = []
#likelihood of row i state goin to col j state
for s1 in states:
    row=[]
    for s2 in states:
        #row s1, col s2 (find incidence of s1 s2)
        prob=0
        total=0
        for sentence in Spanish:
            for i in range(0,len(sentence)):
                if sentence[i]==s1 and i<len(sentence)-1 and sentence[i+1] in states:
                    total+=1
                    if i<len(sentence)-1:
                        if sentence[i+1]==s2:
                            prob+=1
        if not total==0:
            prob=float(prob)/total
        row+=[prob]
    transitionprob += [row]
transitionprob = np.array(transitionprob)

"""
emissionprob = []
#Row i is state i
#Col j is observation j
#Prob of state i being aligned w/ observation j

#REMAKE TOTALS!!!! using observation and not entire english vocab lol
for key in totals:
    #counts amount of alignments to words in the observations
    totalsnum = 0
    for word in observations:
        totalsnum+=alignments[key][word]
    totals[key]=totalsnum

for k in range(0,len(states)):
    row=[]
    for r in range(0,len(observations)):
        incidence=alignments[states[k]][observations[r]]
        totalincidence=totals[states[k]]
        if not totalincidence == 0:
            totalprob = float(incidence)/totalincidence
            row+=[totalprob]
        else:
            row+=[0]
    emissionprob+=[row]

emissionprob = np.array(emissionprob)
"""
#model = hmm.MultinomialHMM(n_components=len(states))
#model.startprob_ = startprobability
#model.transmat_ = transitionprob
#model.emissionprob_ = emissionprob

#print observations
#print "START:",startprobability
#print "TRANSITION",transitionprob
#logprob, trans = model.decode(sequence, algorithm="viterbi")

obs = tuple(observations)
states = tuple(states)
start_p = {}
for i in range(0,len(states)):
    start_p[states[i]] = startprobability[i]
trans_p = {}
for j in range(0,len(states)):
    trans_p[states[j]] = {}
    for k in range(0,len(states)):
        trans_p[states[j]][states[k]] = transitionprob[j][k]
#print emissionprob
#print start_p
#print "/n"
#print trans_p
#print "/n"

"""
emit_p = {}
for l in range(0,len(states)):
    emit_p[states[l]] = {}
    for m in range(0,len(obs)):
        emit_p[states[l]][obs[m]] = emissionprob[l][m]

print "/n"
print emit_p
"""

with open("results.txt", "r") as tdict:
    data = tdict.read()

alignments = eval(data)

vocab = []
for key in alignments:
    for k in alignments[key]:
        vocab+=[k]

vocab = list(set(vocab))

for key1 in alignments:
    for word in vocab:
        if word not in alignments[key1].keys():
            alignments[key1][word] = 0.0

emit_p = alignments
#print alignments
#print pprint.pprint(alignments)

print vocab

#print viterbi(obs,states,start_p,trans_p,emit_p)
"""
obs = ('the', 'blue', 'house')
states = ('la', 'casa','azul')
start_p = {'la': 1.0, 'casa':0.0 ,'azul':0.0}
trans_p = {
   'la': {'la':0.02,'casa':0.8,'azul':0.18},
   'casa': {'la':0.02,'casa':0.03,'azul':0.95},
   'azul': {'la':0.3,'casa':0.6,'azul':0.1}
   }
emit_p = {
   'la': {'the': 0.7, 'blue': 0.2, 'house': 0.1},
   'casa': {'the': 0.1, 'blue': 0.1, 'house': 0.8},
   'azul': {'the': 0.05, 'blue': 0.75, 'house': 0.2},
   }
 """
